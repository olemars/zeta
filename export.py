# -*- coding: utf-8 -*-

import os
import pandas as pd
from parameters import params

def readfile(path):
    with open(path, "rb") as binfile:
        return binfile.read()
    return None

def writecsv(path, data, header):
    df = pd.DataFrame(data)
    basename = os.path.splitext(os.path.basename(path))[0]
    df.to_csv("outfiles/" + basename + ".csv", header=header, index_label="index")
    
def decodeFile(path):
    fparams = params[os.path.basename(path)]
    data = readfile(path)
    if fparams.ptoffset is not None:
       ptdata = data[fparams.ptoffset:fparams.textoffset-fparams.ptoffset]
    else:
        ptdata = None
    if fparams.textoffset is not None:    
        textdata = data[fparams.textoffset:]
    else:
        textdata = data
    csvdata = [s for s in textdata.decode("shift-jis", "replace").split(sep=fparams.eom.decode("shift-jis"))]
    if fparams.eof is not None and csvdata[-1] == fparams.eof.decode("shift-jis"):
        csvdata.pop()
    writecsv(path, csvdata, ["jptext"])
    return csvdata    
    
inpath = "infiles"
for file in os.listdir(inpath):
    if file in params:
        decodeFile(inpath + "/" + file)