# -*- coding: utf-8 -*-


class FileStruct():
    def __init__(self, eom, eol, eof, ptoffset, textoffset):
        self.eom = eom
        self.eol = eol
        self.eof = eof
        self.ptoffset = ptoffset
        self.textoffset = textoffset
        
params = {}
params["EFMES02.DAT"] = FileStruct(eom =b"\x00", eol=b"\x0D\x0A", eof = b"\x1A", ptoffset=None, textoffset=None)
params["EFMES03.DAT"] = FileStruct(eom=b"\x00", eol=b"\x0D\x0A", eof = b"\x1A", ptoffset=None, textoffset=None)
params["AREA0.DAT"] = FileStruct(eom=b"\x00", eol=b"\x0A", eof = None, ptoffset=0x825B, textoffset=0x8B3F)
params["AREA1.DAT"] = FileStruct(eom=b"\x00", eol=b"\x0D\x0A", eof = None, ptoffset=0x77B0, textoffset=0x81E0)
params["AREA2.DAT"] = FileStruct(eom=b"\x00", eol=b"\x0D\x0A", eof = None, ptoffset=0xB724, textoffset=0xC178)